package com.example.lfisc.eval;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendButton(View aView){
        EditText g1Text = findViewById(R.id.gift1);
        String gift1 = g1Text.getText().toString();

        EditText g2Text = findViewById(R.id.gift2);
        String gift2 = g2Text.getText().toString();

        EditText g3Text = findViewById(R.id.gift3);
        String gift3 = g3Text.getText().toString();

        EditText p1Text = findViewById(R.id.p1);
        String p1 = p1Text.getText().toString();

        EditText p2Text = findViewById(R.id.p2);
        String p2 = p2Text.getText().toString();

        EditText p3Text = findViewById(R.id.p3);
        String p3 = p3Text.getText().toString();


        //Erstelle Intent
        Intent intent = new Intent(this,Main2Activity.class);
        //Übertrage werte auf Intent
        intent.putExtra("g1",gift1);
        intent.putExtra("p1", p1);

        intent.putExtra("g2",gift2);
        intent.putExtra("p2", p2);

        intent.putExtra("g3",gift3);
        intent.putExtra("p3", p3);

        startActivity(intent);

    }


    public void resetButton(View aView){
        EditText p1Text = findViewById(R.id.p1);
        p1Text.setText("10");

        EditText p2Text = findViewById(R.id.p2);
        p2Text.setText("10");

        EditText p3Text = findViewById(R.id.p3);
        p3Text.setText("10");

    }
}
