package com.example.lfisc.eval;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();

        if(intent!=null){
            String g1 = intent.getStringExtra("g1");
            String p1 = intent.getStringExtra("p1");

            String g2 = intent.getStringExtra("g2");
            String p2 = intent.getStringExtra("p2");

            String g3 = intent.getStringExtra("g3");
            String p3 = intent.getStringExtra("p3");

            System.out.println(g1);
            System.out.println(p1);
            System.out.println(g2);
            System.out.println(p2);
            System.out.println(g3);
            System.out.println(p3);


            TextView og1 = findViewById(R.id.og1);
            og1.setText(g1);

            TextView op1 = findViewById(R.id.op1);
            op1.setText(p1);

            TextView og2 = findViewById(R.id.og2);
            og2.setText(g2);

            TextView op2 = findViewById(R.id.op2);
            op2.setText(p2);

            TextView og3 = findViewById(R.id.og3);
            og3.setText(g3);

            TextView op3 = findViewById(R.id.op3);
            op3.setText(p3);

            TextView sum = findViewById(R.id.sum);

            int summe = 0;
            String sumout = "0";

            try {
                int price1 = Integer.parseInt(p1);
                int price2 = Integer.parseInt(p2);
                int price3 = Integer.parseInt(p3);
                summe = price1+price2+price3;
                sumout = Integer.toString(summe);
            }catch (Exception e){
                System.err.println("Kein Int");
            }

            sum.setText(sumout);


        }



    }
}
