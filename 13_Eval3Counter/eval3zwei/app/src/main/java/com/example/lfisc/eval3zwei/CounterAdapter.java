package com.example.lfisc.eval3zwei;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CounterAdapter extends ArrayAdapter<Counter> {
    private Context mContext;
    private List<Counter> counterList;

    public CounterAdapter(@NonNull Context context, List<Counter> objects) {
        super(context, 0, objects);
        mContext  = context;
        counterList = objects;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);

        Counter currentCounter = counterList.get(position);

        TextView name = listItem.findViewById(R.id.textView_name);
        name.setText(currentCounter.getName());

        TextView sec = listItem.findViewById(R.id.textView_sec);
        sec.setText(Integer.toString(currentCounter.getCount()));

        return listItem;
    }


}
