package com.example.lfisc.eval3zwei;

public class Counter {
    private String mName;
    private int mCount;

    public Counter(String mName){
        this.mName = mName;
        this.mCount = 0;
    }

    public void up(){
        this.mCount = this.mCount+1;
    }

    public void down(){
        if(this.mCount==0){

        }else{
            this.mCount = this.mCount - 1;
        }
    }

    public String getName(){
        return this.mName;
    }

    public int getCount(){
        return this.mCount;
    }
}
