package com.example.lfisc.eval3zwei;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView lv;
    ArrayList<Counter> counterList;

    CounterAdapter adapter;
    boolean buttonState = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        validateButton();

        lv = (ListView) findViewById(R.id.timerList);
        counterList = new ArrayList<>();

        counterList.add(new Counter("Test"));
        counterList.add(new Counter("Test 2"));

        adapter = new CounterAdapter(this, counterList);
        lv.setAdapter(adapter);

        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Counter counter = (Counter) lv.getItemAtPosition(position);
                if(buttonState){
                    counter.up();
                }
                else{
                    counter.down();
                }

                adapter.notifyDataSetChanged();
            }
        });

    }

    public void onButtonClick(View aView){
        if(buttonState){
            buttonState=false;
        }
        else{
            buttonState=true;
        }

        validateButton();

    }

    public void onButtonAdd(View aView){
        EditText et = findViewById(R.id.editText);
        String name = et.getText().toString();
        counterList.add(new Counter(name));
        adapter.notifyDataSetChanged();
    }

    public void validateButton(){
        Button bt = findViewById(R.id.button2);

        if(buttonState){
            bt.setText("Up");
        }
        else{
            bt.setText("Down");
        }

    }

}
