package com.example.lfisc.exsharedpreflistview;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    SensorManager mSensorManager;
    ListView lv;
    Intent intent = new Intent(this, DetailActivity.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Intent
        intent = new Intent(this, DetailActivity.class);


        //Get all sensors über den sensormanager und speichern in Liste
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        List<Sensor> deviceSensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);

        //simple_list_item.xml muss erstellt werden, siehe layout
        ArrayAdapter<Sensor> adapter = new ArrayAdapter<Sensor>(this,R.layout.simple_list_item,deviceSensors);
        //get List View
        lv = findViewById(R.id.listView);
        //Adapter setzen
        lv.setAdapter(adapter);

        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Sensor o = (Sensor) lv.getItemAtPosition(position);
                System.out.println(o.getType());

                intent.putExtra("type",o.getType());
                startActivity(intent);
            }
        });


    }


}
