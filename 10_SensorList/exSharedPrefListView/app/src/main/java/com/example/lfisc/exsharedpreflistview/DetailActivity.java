package com.example.lfisc.exsharedpreflistview;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailActivity extends AppCompatActivity implements SensorEventListener {

    SensorManager mSensorManager;
    Sensor mSensor;
    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        if(intent!=null){
            int type = intent.getIntExtra("type",0);
            System.out.println(type);

            //SensorManager erstellen
            mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
            //Sensor anhand der ID holen
            mSensor = mSensorManager.getDefaultSensor(type);

            //Sensor beim Listener registrieren
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        String result = "";

        for (int i = 0; i<event.values.length; i++){
           result = result + " Value " + i + " : " + event.values[i] + "\n";
        }

        setText(result);

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void setText(String text){
       tv = findViewById(R.id.detail);
       tv.setText(text);
    }

    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
}
