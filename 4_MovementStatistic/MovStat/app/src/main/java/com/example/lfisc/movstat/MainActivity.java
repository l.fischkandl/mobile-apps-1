package com.example.lfisc.movstat;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener, Runnable {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    float valueX;
    float valueY;
    float valueZ;
    Handler timerHandler;
    int i;

    int countX;
    int countY;
    int countZ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        valueX = Math.abs(event.values[0]); //Math.abs um den Betrag der Zahl zu erhalten
        valueY = Math.abs(event.values[1]);
        valueZ = Math.abs(event.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    public String getHighest(float x, float y, float z){
        float max = Math.max(x,Math.max(y,z));
        String ret = "";
        if (max==x){
            ret = "x";
        }
        else if(max==y){
            ret= "y";
        }
        else if(max==z){
            ret= "z";
        }
        return ret;
    }

    public void setText(String text){
        TextView output = findViewById(R.id.output);
        output.setText(text);
    }


    @Override
    public void run() {
        if(i==12){
            timerHandler.removeCallbacks(this);
            System.out.println("Ende");
            System.out.println(getHighest(countX,countY,countZ));
            String highest = getHighest(countX,countY,countZ);
            setText(highest + "  was most often orientied along the direction of gravity");
        }
        else{
            System.out.println("Now: "+i);
            System.out.println("X "+ valueX);
            System.out.println("Y "+ valueY);
            System.out.println("Z" + valueZ);

            String highest = getHighest(valueX,valueY,valueZ);

            setText("Sec "+i*5+" -> "+highest+" highest");

            if(highest=="x"){
                countX++;
                System.out.println("X highest");
            }
            else if(highest=="y"){
                countY++;
                System.out.println("Y highest");
            }
            else if(highest=="z"){
                countZ++;
                System.out.println("Z highest");
            }
            else{}

            i++;
            timerHandler.postDelayed(this,5000); //warte 5 Sekunden
        }

    }

    public void onButtonClick(View aView){
        setText("Start Analysis...");
        countX = 0;
        countY = 0;
        countZ = 0;
        i = 0;
        timerHandler = new Handler();
        timerHandler.postDelayed(this,0);//wake up in 1000millisecs 1 sec
    }
}
