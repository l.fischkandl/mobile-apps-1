### Exercise Movement Statistics (voluntary)
Use the Acceleration Sensor (not linear Acceleration) to take a statistic which Axis (x,y,z) of the Smartphone was most often orientied along the direction of gravity. 

Use the Handler to gather the "axis of gravity" each 5 seconds and make a statistic covering 60 seconds (hence consisting of 12 samples). After 60 seconds, show on the screen the axis that was most often oriented into the direction of gravity. 