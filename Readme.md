## Codes der Vorlesung Mobile Apps 2018

- [Main TravelApp](1_TravelApp/TravelApp/app/src/main/java/com/example/lfisc/travelapp)
- [Main SensorsApp](2_Sensors/Sensors/app/src/main/java/com/example/lfisc/sensors)
- [Main TheftAlert](3_TheftAlert/TheftAlert/main/java/com/example/elaspix/theftalert)
- [Main MovStatistic](4_MovementStatistic/MovStat/app/src/main/java/com/example/lfisc/movstat/)
- [Main GyroRunTemplate](5_GyroRunTemplate/GyroGame/app/src/main/java/com/example/lfisc/gyrogame)
- [Main GyroGame](6_GyroGame/GyroGame/app/src/main/java/com/example/lfisc/gyrogame)
- [Main Eval1 Geschenkeliste](7_Eval1/Eval/app/src/main/java/com/example/lfisc/eval)
- [Main Eval2 Movement Tracker](8_Eval2/eval2/app/src/main/java/com/example/lfisc/eval2)
- [Main Vorlesung Store GUI and List](9_StoreGUIandList/main/java/com/example/elaspix/lifecycletest)
- [Main Sensor List](10_SensorList/exSharedPrefListView/app/src/main/java/com/example/lfisc/exsharedpreflistview)
- [Main Diary List](11_Diary/MainDiary/app/src/main/java/com/example/lfisc/maindiary)    
- [Main Eval 3 Timer](12_Eval3Timer/eval3/app/src/main/java/com/example/lfisc/eval3)
- [Main Eval 3 Counter](13_Eval3Counter/eval3zwei/app/src/main/java/com/example/lfisc/eval3zwei)



