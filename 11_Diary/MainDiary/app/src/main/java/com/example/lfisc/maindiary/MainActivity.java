package com.example.lfisc.maindiary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    HashMap hmap;
    ListView lv;
    SharedPreferences mPrefs;
    Intent intent;
    SharedPreferences.OnSharedPreferenceChangeListener listener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent(this, TextActivity.class);

        //Erstellen der Hashmap

        hmap = new HashMap<String, String>();

        //Füllen der Hashmap

        mPrefs = getSharedPreferences("prefs", MODE_PRIVATE);
        Gson gson = new Gson();
        //get from shared prefs
        String storedHashMapString = mPrefs.getString("diary", "oopsDintWork");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        hmap = gson.fromJson(storedHashMapString, type);

        //Füllen der Liste

        fillList();

    }

    public void onPause(){
        super.onPause();
        System.out.print("onPause");

    }

    public void onButtonClick(View aView){
        intent.putExtra("read",false);
        startActivity(intent);
    }

    public void onResume(){
        super.onResume();
        System.out.println("onResume");
    }

    public void fillList(){

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item, new ArrayList<String>(hmap.keySet()));
        lv = findViewById(R.id.list);
        lv.setAdapter(adapter);

        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String day = (String) lv.getItemAtPosition(position);
                intent.putExtra("read",true);
                intent.putExtra("day",day);
                startActivity(intent);
            }
        });

    }


}
