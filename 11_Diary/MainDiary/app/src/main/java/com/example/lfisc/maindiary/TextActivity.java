package com.example.lfisc.maindiary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;

public class TextActivity extends AppCompatActivity {

    Intent intent;
    boolean modus;
    String day;
    SharedPreferences mPrefs;
    HashMap hmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text);

        intent = getIntent();

        mPrefs = getSharedPreferences("prefs", MODE_PRIVATE);
        //new GSON
        Gson gson = new Gson();
        //get from shared prefs
        String storedHashMapString = mPrefs.getString("diary", "oopsDintWork");
        java.lang.reflect.Type type = new TypeToken<HashMap<String, String>>(){}.getType();
        hmap = gson.fromJson(storedHashMapString, type);

        if(intent!=null){
            modus = intent.getBooleanExtra("read",false);
            day  = intent.getStringExtra("day");


            if(modus==true){
                setH(day);
                setTextField((String) hmap.get(day));
                setButtonText("Back");
            }

            //write
            else {
                day = getDay();
                setH(day);
                setButtonText("Save");

            }



        }
    }

    public void setH(String text){
        TextView tv = findViewById(R.id.h);
        tv.setText(text);
    }

    public void setTextField(String text){
        EditText et = findViewById(R.id.editText);
        et.setText(text);
        et.setFocusable(false);
    }

    public void setButtonText(String text) {
        Button button = findViewById(R.id.button2);
        button.setText(text);
    }

    public String getDay(){
        String result = "";
        result = "Day "+(hmap.size()+1);
        return result;
    }

    public void onButtonClick(View aView){

        EditText et = findViewById(R.id.editText);

        if(modus==false){
            hmap.put(day,et.getText().toString());

            //save in shared prefs
            mPrefs = getSharedPreferences("prefs", MODE_PRIVATE);
            //für Gson in Module:app build.gradle folgendes adden:  implementation 'com.google.code.gson:gson:2.8.5'
            Gson gson = new Gson();
            String hashMapString = gson.toJson(hmap);

            //Einfügen in Shared Prefs
            mPrefs.edit().putString("diary", hashMapString).apply();

        }

        Intent backintent = new Intent(this, MainActivity.class);
        startActivity(backintent);
    }


}

