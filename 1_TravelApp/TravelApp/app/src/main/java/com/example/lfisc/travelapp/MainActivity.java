package com.example.lfisc.travelapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onMeinButton(View aView){

        //Lese werte aus Formular
        EditText startdateText = findViewById(R.id.startdate);
        String startdate = startdateText.getText().toString();

        EditText enddateText = findViewById(R.id.enddate);
        String enddate = enddateText.getText().toString();

        EditText numberofPeopleText = findViewById(R.id.numberop);
        String numberofPeople = numberofPeopleText.getText().toString();

        Spinner countryText = findViewById(R.id.country);
        String country = countryText.getSelectedItem().toString();

        //Erstelle Intent
        Intent intent = new Intent(this,Main2Activity.class);
        //Übertrage werte auf Intent
        intent.putExtra("startdate",startdate);
        intent.putExtra("enddate", enddate);
        intent.putExtra("numberofp",numberofPeople);
        intent.putExtra("country",country);

        startActivity(intent);

    }
}
