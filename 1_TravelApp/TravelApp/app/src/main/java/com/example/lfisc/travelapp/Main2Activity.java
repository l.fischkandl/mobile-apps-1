package com.example.lfisc.travelapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class Main2Activity extends Activity {

    String startdate;
    String enddate;
    String numberofPeople;
    String country;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();

        if(intent!=null){
            startdate = intent.getStringExtra("startdate");
            enddate  = intent.getStringExtra("enddate");
            numberofPeople = intent.getStringExtra("numberofp");
            country = intent.getStringExtra("country");




        }


    }

    public void ButtonToReview(View aView){

        //Lese werte aus Formular
        EditText NameText = findViewById(R.id.name);
        String name = NameText.getText().toString();

        EditText mailText = findViewById(R.id.mail);
        String mail = mailText.getText().toString();

        EditText phoneText = findViewById(R.id.phone);
        String phone = phoneText.getText().toString();


        //Erstelle Intent
        Intent intentSummary = new Intent(this,Main3Activity.class);

        //Übertrage werte auf Intent
        intentSummary.putExtra("name",name);
        intentSummary.putExtra("mail", mail);
        intentSummary.putExtra("phone",phone);

        //Übertrage werte auf Intent
        intentSummary.putExtra("startdate",startdate);
        intentSummary.putExtra("enddate", enddate);
        intentSummary.putExtra("numberofp",numberofPeople);
        intentSummary.putExtra("country",country);

        startActivity(intentSummary);

        System.out.println("Teeest");

    }

}
