package com.example.lfisc.travelapp;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class Main3Activity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);



        Intent intent = getIntent();

        String startdate = intent.getStringExtra("startdate");
        String enddate  = intent.getStringExtra("enddate");
        String numberofPeople = intent.getStringExtra("numberofp");
        String country = intent.getStringExtra("country");

        String name = intent.getStringExtra("name");
        String mail  = intent.getStringExtra("mail");
        String phone = intent.getStringExtra("phone");

        TextView eins = findViewById(R.id.eins);
        eins.setText(startdate);

        TextView zwei = findViewById(R.id.zwei);
        zwei.setText(enddate);

        TextView drei = findViewById(R.id.drei);
        drei.setText(numberofPeople);

        TextView vier = findViewById(R.id.vier);
        vier.setText(country);

        TextView funf = findViewById(R.id.funf);
        funf.setText(name);

        TextView sechs = findViewById(R.id.sechs);
        sechs.setText(mail);

        TextView sieben = findViewById(R.id.sieben);
        sieben.setText(phone);

    }

}
