### Travel App
Create an App that collects travel data to request a quote for a summer travel (the request is not part of the exercise). 
The required travel data is  
- Day of start and day of travel end
- Target Country
- Number of adults / kids / pets
- Affordable Price range (from..to)
- Email   
Use two or more screens (Android-Acitivites) and appropriate GUI-Elements (Android-Views) to collect the data. Use a last screen (Android-Activity) to show a one-page summary report of the submitted data.
Note: to pass info to other activities use putExtra at Intent creation