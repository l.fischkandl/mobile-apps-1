package com.example.elaspix.theftalert;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity implements SensorEventListener {

    boolean mOnMonitor;//stores whether the App is sharp or not
    Sensor mAccelerator;//the Linear Accelerator Sensor
    SensorManager mSensorManager;
    long mLastTimeCode;//the timecode of the last onSensorChanged-Call
    double sTotal;//the total space in meters
    double sProximity;//the tolerance in meters, given by the user

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mOnMonitor = false;//set app to not sharp
        //get the instances of sensor and sensor manager
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mAccelerator = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }

    /**
     * butten event listener called when the user hits "monitor" button
     * @param aView
     */
    public void onMonitor(View aView) {
        mOnMonitor = true;
        sTotal = 0;//the total space is set to zero
        EditText editText = findViewById(R.id.editText);//read the proximity tolerance value
        sProximity = Double.parseDouble(editText.getText().toString());
        mLastTimeCode = 0;//set to zero allows to detect a first run of the onSensorChanged Method
        System.out.println("onMonitor");
        //power on the sensor
        mSensorManager.registerListener(this, mAccelerator, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (mOnMonitor) {
            if (mLastTimeCode == 0) {
                //in case the method runs the first time, get the last timestamp as reference
                mLastTimeCode = event.timestamp;
            }

            //compute acceleration in m/s*s
            double a = Math.sqrt(Math.pow(event.values[0], 2) +
                    Math.pow(event.values[1], 2) +
                    Math.pow(event.values[2], 2));

            //the nanoseconds pasted since last call of onSensorChanged
            long deltaTime = event.timestamp - mLastTimeCode;

            //store the actual time as new lastTimeCode
            mLastTimeCode = event.timestamp;

            //compute the passed time in seconds
            double t = deltaTime / 1000000000.0;//in seconds

            //compute the velocity
            double v = a * t;//meter per second

            //compute the space
            double s = v * t;//meter

            //accumulate the space since last call to the total space
            sTotal = sTotal + s;

            System.out.println("sTotal=" + sTotal + "m");

            //check if the tolerance is exceeded
            if (sTotal > sProximity) {
                System.out.println("ALARM");
                mOnMonitor=false;//make sure that the app stops outputting

                //make the screen red
                View someView = findViewById(R.id.editText);
                View root = someView.getRootView();
                root.setBackgroundColor(Color.RED);
            }
        }


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
