### Aufgabe Counter

#### Erstes Element: Klasse Timer
Die Klasse Timer enthält die Informationen eines jeden Timers. 
Mit verschiedenen Funktionen: Läuft der Timer? Herunterzählen des Timers. 

#### Zweites Element: ArrayListe mit Timer
Die Timer werden in einer ArrayListe gespeichert. 

#### Drittes Element: Customized Adapter
Für den Costumized Adapter wird eine neue Klasse erstellt, welche die Klasse ArrayAdapter erweitert. 
Hier ist es wichtig ein customized Layout File für den List View zu setzen. 
[Layout list item](12_Eval3Timer/eval3/app/src/main/res/layout/list_item.xml)

#### Viertes Element: Setzen des Adapters für den ListView 

#### Fünftes Element: Handler
Implementieren des Runnable Interfaces und der Methode run. 
Die Methode geht jede Sekunde die ArrayListe mit den Timern durch und schaut, ob der Timer läuft. Wenn ja, dann wird der Timer heruntergezählt.

