package com.example.lfisc.eval3;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements Runnable{

    ListView lv;
    ArrayList<Timer> timerList;
    //ArrayAdapter<Timer> adapter;
    TimerAdapter adapter;
    Handler timerHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lv = (ListView) findViewById(R.id.timerlist);
        timerList = new ArrayList<>();

        timerList.add(new Timer("Test",1));
        timerList.add(new Timer("Test 2",2));
        timerList.add(new Timer("Test 3",3));


        //adapter = new ArrayAdapter<Timer>(this, R.layout.simple_list_item, timerList);
        adapter = new TimerAdapter(this, timerList);
        lv.setAdapter(adapter);


        timerHandler = new Handler();
        timerHandler.postDelayed(this,0);//wake up in 1000millisecs 1 sec

        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Timer timer = (Timer) lv.getItemAtPosition(position);
                timer.changeState();
            }
        });
    }

    public void onButtonClick(View aView){
        EditText name = findViewById(R.id.tName);
        EditText sec = findViewById(R.id.tSec);
        String res = sec.getText().toString();
        int seconds = Integer.parseInt(res);

        timerList.add(new Timer(name.getText().toString(),seconds));
        adapter.notifyDataSetChanged();

    }

    @Override
    public void run() {
        for (Timer timer : timerList){
            if (timer.getState()==true){
                timer.timerDown();
            }
        }
        adapter.notifyDataSetChanged();
        timerHandler.postDelayed(this,1000);//wake up in 1000millisecs 1 sec
    }
}
