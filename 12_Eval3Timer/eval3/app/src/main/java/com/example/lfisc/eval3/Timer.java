package com.example.lfisc.eval3;

public class Timer {
    private String mName;
    private int mSeconds;
    private boolean run;

    public Timer(String mName, int mSeconds){
        this.mName = mName;
        this.mSeconds = mSeconds;
        this.run = false;
    }

    public String getmName(){
        return this.mName;
    }

    public int getmSeconds(){
        return this.mSeconds;
    }

    public boolean getState(){
        return this.run;
    }

    public void changeState(){
        if(this.run==false){
            this.run=true;
        }
        else{
            this.run = false;
        }
    }

    public void run(){
        this.run = true;
    }

    public void stop(){
        this.run = false;
    }

    public void timerDown(){
        if (this.mSeconds !=0){
            this.mSeconds = this.mSeconds-1;
        }

    }

}
