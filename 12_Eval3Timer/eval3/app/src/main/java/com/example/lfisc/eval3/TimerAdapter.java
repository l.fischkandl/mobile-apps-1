package com.example.lfisc.eval3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class TimerAdapter extends ArrayAdapter<Timer>{

    private Context mContext;
    private List<Timer> timerList;

    public TimerAdapter(@NonNull Context context, @NonNull List<Timer> objects) {
        super(context, 0, objects);
        mContext = context;
        timerList = objects;
    }

    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.list_item,parent,false);

        Timer currentTimer = timerList.get(position);

        TextView name = listItem.findViewById(R.id.textView_name);
        name.setText(currentTimer.getmName());

        TextView sec = listItem.findViewById(R.id.textView_sec);
        sec.setText(Integer.toString(currentTimer.getmSeconds()));

        return listItem;
    }
}