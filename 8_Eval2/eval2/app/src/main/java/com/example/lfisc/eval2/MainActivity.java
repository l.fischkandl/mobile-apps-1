package com.example.lfisc.eval2;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener, Runnable {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    float valueX;
    float valueY;
    float valueZ;
    Handler timerHandler;
    int i;
    boolean testref;

    float[] ref = new float[30];
    float[] test = new float[30];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        valueX = event.values[0]; //Math.abs um den Betrag der Zahl zu erhalten
        valueY = event.values[1];
        valueZ = event.values[2];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public float getGesamtBeschleunigung(){
        double val = Math.sqrt(valueX*valueX + valueY*valueY + valueZ*valueZ);
        System.out.println("Val "+val);
        return (float)val;
    }

    @Override
    public void run() {
        if(i==30){
            timerHandler.removeCallbacks(this);
            if(testref){
                //System.out.println(getDifferenz());
                //getDifferenz();
                setText("Testlauf fertig | Summe der Differenzen "+getDifferenz());
            }
            else {
                setText("Referenzerstellung fertig");
            }
        }
        else{
            if(testref){
                test[i] = getGesamtBeschleunigung();
                setText("Test | Sec " + i + " Val "+test[i]);
                i++;
                timerHandler.postDelayed(this,1000);//wake up in 1000millisecs 1 sec
            }
            else{
                ref[i] = getGesamtBeschleunigung();
                setText("Ref | Sec " + i + " Val "+ref[i]);
                System.out.println("Ref "+i);
                i++;

                timerHandler.postDelayed(this,1000);//wake up in 1000millisecs 1 sec
            }

        }

    }

    public void onButtonStarteRef(View aView){
        setText("Starte Referenz");
        i = 0;
        testref = false;

        timerHandler = new Handler();
        timerHandler.postDelayed(this,1000);//wake up in 1000millisecs 1 sec

    }

    public void onButtonStarteTest (View aView){
        setText("Starte Test");
        i = 0;
        testref = true;

        timerHandler = new Handler();
        timerHandler.postDelayed(this,1000);//wake up in 1000millisecs 1 sec

    }


    public float getDifferenz(){
        float sum = 0;
           for(int i = 0; i < ref.length; i++){
               float dif = Math.abs(ref[i])-Math.abs(test[i]);
               System.out.println(dif);
              sum = sum + Math.abs(dif);
              System.out.println("Ref: "+ ref[i]);
              System.out.println("Test: "+test[i]);
           }
           return sum;
    }



    public void setText(String text){
        TextView output = findViewById(R.id.v);
        output.setText(text);

    }

}
