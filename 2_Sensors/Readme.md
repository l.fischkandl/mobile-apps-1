### Aufgabe 1
Erstellen Sie eine Android-App, welche st�ndig die St�rke*2 der Beschleunigung misst (mit Hilfe des Linear-Acceleration-Sensors*1) und als Wort nach folgender Funktion ausgibt: "keine" / "gering" / "mittel" / "viel"  wenn die Gesamtbeschleunigung in den Intervallen 0-0.5 m/s�  / 0.5-3 m/s� / 3-5 m/s�  �ber 5 m/s� ist. Die Ausgabe soll in einer TextView mit gro�er Schrift auf dem Display erfolgen.
*1: der Linear-Acceleration Sensor rechnet die Erdbeschleunigung mit Hilfe des Gravity-Sensors aus dem Acceleration-Sensor heraus
*2: die St�rke eines Vektors ergibt sich durch den Satz des Pythagoras

### Aufgabe 2
Die obige Aufgabe wird wie folgt abge�ndert: statt einer Ausgabe in nur einer Textview soll die St�rke der Beschleunigung (in Wortform) in einer ListView eingetragen werden, die sich einmal pro Sekunde updated und in freie (bzw. untere) Zeilen den aktuellen Wert hineinschreibt (die alten Werte rutschen dann hinauf, so dass man immer die letzten 10 W�rter sehen kann).

