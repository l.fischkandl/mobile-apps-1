package com.example.lfisc.sensors;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Main3Activity extends AppCompatActivity implements SensorEventListener {

    List<String> array = new ArrayList<String>();
    ArrayAdapter adapter;


    private SensorManager mSensorManager;
    private Sensor mAccelerometer;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        ListView lv = findViewById(R.id.lv);
        //Es muss eine neue Layout Datei hinzugefügt werden, die ein List Element enthält R.layout.list_item
        adapter = new ArrayAdapter<String>(this,R.layout.list_item,R.id.list_item_liste_view,array);
        lv.setAdapter(adapter);


        //Init Sensors
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);

    }


    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, 1000000, 1000000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public String converToString(double length)
    {
        if (length>5) return "Viel";
        if (length>3) return "Mittle";
        if (length>0.5) return "Gering";
        return "Keine";
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double acc=Math.sqrt(event.values[0]*event.values[0]+event.values[1]*event.values[1]
                +event.values[2]*event.values[2]);

        //TextView output = findViewById(R.id.outp);
        //output.setText(converToString(acc));
        System.out.println(converToString(acc));
       array.add(converToString(acc));

        //List View updaten
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


}
