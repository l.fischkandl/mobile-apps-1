package com.example.lfisc.sensors;

import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    public String converToString(double length)
    {
        if (length>5) return "Viel";
        if (length>3) return "Mittle";
        if (length>0.5) return "Gering";
        return "Keine";
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        double acc=Math.sqrt(event.values[0]*event.values[0]+event.values[1]*event.values[1]
                +event.values[2]*event.values[2]);

        TextView output = findViewById(R.id.outp);
        output.setText(converToString(acc));
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onButtonClick(View aView){
        Intent intent = new Intent(this,Main3Activity.class);
        startActivity(intent);
    }
}
