package com.example.lfisc.sensors;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;

//Implements SensorEventListener, um die zwei Methoden onSensorChanged und onAccuracyChanged
public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        System.out.println(event.values[0]);
        System.out.println(event.values[1]);
        System.out.println(event.values[2]);

        TextView x = findViewById(R.id.x);
        x.setText(Float.toString(event.values[0]));

        TextView y = findViewById(R.id.y);
        y.setText(Float.toString(event.values[1]));

        TextView z = findViewById(R.id.z);
        z.setText(Float.toString(event.values[2]));

        System.out.println(event.values[0]);
        System.out.println(event.values[1]);
        System.out.println(event.values[2]);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public void onButtonClick(View aView){
        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }

    public void onButtonTheft(View aView){
        Intent intent = new Intent(this, Main4Activity.class);
        startActivity(intent);
    }
}
