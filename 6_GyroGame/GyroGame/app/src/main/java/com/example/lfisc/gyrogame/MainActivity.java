package com.example.lfisc.gyrogame;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SensorEventListener{

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    float valueX;
    float valueY;
    float valueZ;
    int round;
    double timePerRound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        valueX = event.values[0];
        valueY = event.values[1];
        valueZ = event.values[2];

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    public void runGame() {

        setColor("white");

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                final int leftorRight = generateRound();

                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if(testResult(leftorRight,valueX)){
                            round++;
                            timePerRound=timePerRound-100;
                            runGame();
                        }
                        else{
                            setText("Runden "+round);
                            setColor("red");
                        }

                    }
                }, (long)timePerRound);


            }
        }, (long)200);





    }

    public void setText(String text){
        TextView output = findViewById(R.id.output);
        output.setText(text);

    }

    public void setColor(String color){
        ConstraintLayout li = findViewById(R.id.constraintLayout);

        if(color=="red"){
            li.setBackgroundColor(Color.parseColor("red"));
        }
        else if(color=="green"){
            li.setBackgroundColor(Color.parseColor("green"));
        }
        else if(color=="yellow"){
            li.setBackgroundColor(Color.parseColor("yellow"));
        }
        else if(color=="blue"){
            li.setBackgroundColor(Color.parseColor("blue"));
        }
        else if(color=="white"){
            li.setBackgroundColor(Color.parseColor("white"));
        }

    }

    public int generateRandom(){
        int zufallszahl = (int)(Math.random()*2);
        return zufallszahl;
    }

    public int generateRound(){
        int zufallszahl = generateRandom();
        if(zufallszahl==0){
            setText("RECHTS");
            setColor("blue");
        }
        else if(zufallszahl==1){
            setText("LINKS");
            setColor("yellow");
        }
        return zufallszahl;
    }

    public boolean testResult(int round, float x){
        boolean ret = false;
        if(round==0 && x<0){
            ret = true;
        }
        else if (round==1 && x>0){
            ret = true;
        }
        return ret;
    }

    public void onButtonClick(View aView){
        round = 0;
        timePerRound = 2000;
        runGame();

        Button but = findViewById(R.id.button);
        but.setText("Restart Game");

    }
}
