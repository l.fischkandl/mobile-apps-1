package com.example.elaspix.lifecycletest;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {


    EditText mEditText1;
    EditText mEditText2;
    EditText mEditText3;
    SharedPreferences mPrefs;

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("onReStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop");
        String text1=mEditText1.getText().toString();
        String text2=mEditText2.getText().toString();
        String text3=mEditText3.getText().toString();
        SharedPreferences.Editor e=mPrefs.edit();
        e.putString("text1",text1);
        e.putString("text2",text2);
        e.putString("text3",text3);
        e.commit();
        System.out.println(text1+" "+text2+" "+text3);
    }

    @Override
    protected void onDestroy() {
        System.out.println("onDestroy");
        super.onDestroy();
        System.out.println("onDestroy2");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        System.out.println("onCreate");
        mPrefs=getSharedPreferences("nachrichtenaustausch",MODE_PRIVATE);
        mEditText1=findViewById(R.id.editText1);
        mEditText2=findViewById(R.id.editText2);
        mEditText3=findViewById(R.id.editText3);

        String text1=mPrefs.getString("text1","eins");
        String text2=mPrefs.getString("text2","zwei");
        String text3=mPrefs.getString("text3","drei");

        mEditText1.setText(text1);
        mEditText2.setText(text2);
        mEditText3.setText(text3);

    }

    public void onShowButtonPress(View aView)
    {
        Intent showActivity=new Intent(this,ShowActivity.class);
        startActivity(showActivity);
    }
}
