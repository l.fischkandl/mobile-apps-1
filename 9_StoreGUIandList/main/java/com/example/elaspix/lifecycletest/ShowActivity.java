package com.example.elaspix.lifecycletest;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class ShowActivity extends Activity {

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart der ShowActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume der ShowActivity");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show);


        SharedPreferences prefs =getSharedPreferences("nachrichtenaustausch",
                MODE_PRIVATE);
        String text1=prefs.getString("text1","noch kein Eintrag");
        String text2=prefs.getString("text2","noch kein Eintrag");
        String text3=prefs.getString("text3","noch kein Eintrag");

        //textView.setText("der ausgetauschte Text ist"+text1+text2+text3);

        ArrayList<String> array=new ArrayList<>();
        array.add(text1);
        array.add(text2);
        array.add(text3);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                array);
        ListView listView=findViewById(R.id.listView);

        listView.setAdapter(adapter);

        System.out.println("onCreate der ShowActivity");

    }
}
